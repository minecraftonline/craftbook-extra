/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.util.data;

import com.minecraftonline.data.ChairData;
import com.minecraftonline.data.ChairDataBuilder;
import com.minecraftonline.data.ChairExitLocationData;
import com.minecraftonline.data.ChairExitLocationDataBuilder;
import com.minecraftonline.data.ChairHealData;
import com.minecraftonline.data.ChairHealDataBuilder;
import com.minecraftonline.data.ExtraICLinesData;
import com.minecraftonline.data.ExtraICLinesDataBuilder;
import com.minecraftonline.data.ICDebugStickData;
import com.minecraftonline.data.ICDebugStickDataBuilder;
import com.minecraftonline.data.ICDebugStickModeData;
import com.minecraftonline.data.ICDebugStickModeDataBuilder;
import com.minecraftonline.data.ImmutableChairData;
import com.minecraftonline.data.ImmutableChairExitLocationData;
import com.minecraftonline.data.ImmutableChairHealData;
import com.minecraftonline.data.ImmutableExtraICLinesData;
import com.minecraftonline.data.ImmutableICDebugStickData;
import com.minecraftonline.data.ImmutableICDebugStickModeData;
import com.minecraftonline.data.ImmutablePlacedByAdminData;
import com.minecraftonline.data.PlacedByAdminData;
import com.minecraftonline.data.PlacedByAdminDataBuilder;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.blockbags.EmbeddedBlockBag;
import com.sk89q.craftbook.sponge.util.data.builder.BlockBagDataManipulatorBuilder;
import com.sk89q.craftbook.sponge.util.data.builder.EmbeddedBlockBagDataBuilder;
import com.sk89q.craftbook.sponge.util.data.builder.ICDataManipulatorBuilder;
import com.sk89q.craftbook.sponge.util.data.builder.KeyLockDataBuilder;
import com.sk89q.craftbook.sponge.util.data.builder.LastPowerDataManipulatorBuilder;
import com.sk89q.craftbook.sponge.util.data.builder.NamespaceDataBuilder;
import com.sk89q.craftbook.sponge.util.data.immutable.ImmutableBlockBagData;
import com.sk89q.craftbook.sponge.util.data.immutable.ImmutableEmbeddedBlockBagData;
import com.sk89q.craftbook.sponge.util.data.immutable.ImmutableICData;
import com.sk89q.craftbook.sponge.util.data.immutable.ImmutableKeyLockData;
import com.sk89q.craftbook.sponge.util.data.immutable.ImmutableLastPowerData;
import com.sk89q.craftbook.sponge.util.data.immutable.ImmutableNamespaceData;
import com.sk89q.craftbook.sponge.util.data.mutable.BlockBagData;
import com.sk89q.craftbook.sponge.util.data.mutable.EmbeddedBlockBagData;
import com.sk89q.craftbook.sponge.util.data.mutable.ICData;
import com.sk89q.craftbook.sponge.util.data.mutable.KeyLockData;
import com.sk89q.craftbook.sponge.util.data.mutable.LastPowerData;
import com.sk89q.craftbook.sponge.util.data.mutable.NamespaceData;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataRegistration;

public class CraftBookData {

    public static void registerData() {
        // This registers the keys.
        new CraftBookKeys();

        // Generic Data
        DataRegistration<LastPowerData, ImmutableLastPowerData> lastPowerData =
                DataRegistration.builder()
                        .dataClass(LastPowerData.class)
                        .immutableClass(ImmutableLastPowerData.class)
                        .builder(new LastPowerDataManipulatorBuilder())
                        .manipulatorId("last_power")
                        .dataName("LastPower")
                        .buildAndRegister(CraftBookPlugin.spongeInst().container);

        Sponge.getDataManager().registerLegacyManipulatorIds("com.sk89q.craftbook.sponge.util.data.mutable.LastPowerData", lastPowerData);

        // IC Data
        DataRegistration<ICData, ImmutableICData> icData =
                DataRegistration.builder()
                        .dataClass(ICData.class)
                        .immutableClass(ImmutableICData.class)
                        .builder(new ICDataManipulatorBuilder())
                        .manipulatorId("ic")
                        .dataName("IC")
                        .buildAndRegister(CraftBookPlugin.spongeInst().container);

        Sponge.getDataManager().registerLegacyManipulatorIds("com.sk89q.craftbook.sponge.util.data.mutable.ICData", icData);

        // Area Data
        DataRegistration<NamespaceData, ImmutableNamespaceData> namespaceData =
                DataRegistration.builder()
                        .dataClass(NamespaceData.class)
                        .immutableClass(ImmutableNamespaceData.class)
                        .builder(new NamespaceDataBuilder())
                        .manipulatorId("namespace")
                        .dataName("Namespace")
                        .buildAndRegister(CraftBookPlugin.spongeInst().container);

        Sponge.getDataManager().registerLegacyManipulatorIds("com.sk89q.craftbook.sponge.util.data.mutable.NamespaceData", namespaceData);

        // BlockBag Data
        Sponge.getDataManager().registerBuilder(EmbeddedBlockBag.class, new EmbeddedBlockBag.EmbeddedBlockBagBuilder());

        DataRegistration<EmbeddedBlockBagData, ImmutableEmbeddedBlockBagData> embeddedBlockBagData =
                DataRegistration.builder()
                        .dataClass(EmbeddedBlockBagData.class)
                        .immutableClass(ImmutableEmbeddedBlockBagData.class)
                        .builder(new EmbeddedBlockBagDataBuilder())
                        .manipulatorId("embedded_blockbag")
                        .dataName("EmbeddedBlockBag")
                        .buildAndRegister(CraftBookPlugin.spongeInst().container);

        Sponge.getDataManager().registerLegacyManipulatorIds("com.sk89q.craftbook.sponge.mechanics.blockbags.data.EmbeddedBlockBagData", embeddedBlockBagData);

        DataRegistration<BlockBagData, ImmutableBlockBagData> blockBagData =
                DataRegistration.builder()
                        .dataClass(BlockBagData.class)
                        .immutableClass(ImmutableBlockBagData.class)
                        .builder(new BlockBagDataManipulatorBuilder())
                        .manipulatorId("blockbag")
                        .dataName("BlockBag")
                        .buildAndRegister(CraftBookPlugin.spongeInst().container);

        Sponge.getDataManager().registerLegacyManipulatorIds("com.sk89q.craftbook.sponge.mechanics.blockbags.data.BlockBagData", blockBagData);

        // Hidden Switch Data
        DataRegistration.builder()
                .dataClass(KeyLockData.class)
                .immutableClass(ImmutableKeyLockData.class)
                .builder(new KeyLockDataBuilder())
                .manipulatorId("key_lock")
                .dataName("KeyLock")
                .buildAndRegister(CraftBookPlugin.spongeInst().container);

        // Extra IC Lines Data
        DataRegistration.builder()
                .dataClass(ExtraICLinesData.class)
                .immutableClass(ImmutableExtraICLinesData.class)
                .builder(new ExtraICLinesDataBuilder())
                .id("extra_ic_lines")
                .name("ExtraICLines")
                .build();

        // Placed by admin data
        DataRegistration.builder()
                .dataClass(PlacedByAdminData.class)
                .immutableClass(ImmutablePlacedByAdminData.class)
                .builder(new PlacedByAdminDataBuilder())
                .id("placed_by_admin")
                .name("PlacedByAdmin")
                .build();

        DataRegistration.builder()
                .dataClass(ICDebugStickData.class)
                .immutableClass(ImmutableICDebugStickData.class)
                .builder(new ICDebugStickDataBuilder())
                .id("ic_debug_stick")
                .name("ICDebugStick")
                .build();

        DataRegistration.builder()
                .dataClass(ICDebugStickModeData.class)
                .immutableClass(ImmutableICDebugStickModeData.class)
                .builder(new ICDebugStickModeDataBuilder())
                .id("ic_debug_stick_mode")
                .name("ICDebugStickMode")
                .build();

        DataRegistration.builder()
                .dataClass(ChairData.class)
                .immutableClass(ImmutableChairData.class)
                .builder(new ChairDataBuilder())
                .id("craftbook_chair")
                .name("CraftbookChair")
                .build();

        DataRegistration.builder()
                .dataClass(ChairExitLocationData.class)
                .immutableClass(ImmutableChairExitLocationData.class)
                .builder(new ChairExitLocationDataBuilder())
                .id("craftbook_chair_exit_location")
                .name("CraftbookChairExitLocation")
                .build();

        DataRegistration.builder()
                .dataClass(ChairHealData.class)
                .immutableClass(ImmutableChairHealData.class)
                .builder(new ChairHealDataBuilder())
                .id("craftbook_healing_chair")
                .name("CraftbookHealingChair")
                .build();
    }
}
