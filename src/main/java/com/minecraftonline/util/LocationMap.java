/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util;

import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.storage.ChunkLayout;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LocationMap<V> {

    private final Map<UUID, Map<Vector3i, V>> map = new HashMap<>();

    public V get(Location<World> loc) {
        Map<Vector3i, V> innerMap = map.get(loc.getExtent().getUniqueId());
        if (innerMap == null) {
            return null;
        }
        return innerMap.get(loc.getBlockPosition());
    }

    public V put(Location<World> loc, V value) {
        Map<Vector3i, V> innerMap = map.computeIfAbsent(loc.getExtent().getUniqueId(), k -> new HashMap<>());
        return innerMap.put(loc.getBlockPosition(), value);
    }

    public V remove(UUID worldId, Vector3i pos) {
        Map<Vector3i, V> innerMap = map.get(worldId);
        if (innerMap == null) {
            return null;
        }
        return innerMap.remove(pos);
    }

    public void removeChunk(UUID worldId, Vector3i chunkPos) {
        trimDeadWorlds();
        Map<Vector3i, V> innerMap = map.get(worldId);
        if (innerMap == null) {
            return;
        }
        ChunkLayout chunkLayout = Sponge.getServer().getChunkLayout();
        innerMap.keySet().removeIf(worldPos -> chunkLayout.isInChunk(
                worldPos.getX(), worldPos.getY(), worldPos.getZ(),
                chunkPos.getX(), chunkPos.getY(), chunkPos.getZ()));
    }

    /**
     * Deletes any worlds that no longer exist
     */
    private void trimDeadWorlds() {
        map.keySet().removeIf(uuid -> !Sponge.getServer().getWorld(uuid).isPresent());
    }
}
