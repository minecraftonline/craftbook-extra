/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing;

import com.minecraftonline.util.ParsingConfiguration;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Identifiable;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class BandLineParser extends StringLineParser {

    private final boolean enforcePattern;

    private static final Pattern BAND_PATTERN = Pattern.compile("[^:]+");

    public BandLineParser(@Nullable Text helpMessage, Consumer<String> binder, boolean enforcePattern) {
        super(helpMessage, binder, false);
        this.enforcePattern = enforcePattern;
    }

    @Override
    public void parse(String line, @Nullable Subject cause) throws Parameter.ParsingException, InvalidICException {
        line = line.trim();
        if (this.enforcePattern && !BAND_PATTERN.matcher(line).matches()) {
            throw new Parameter.ParsingException("Band must match " + BAND_PATTERN);
        }
        super.parse(line, cause);
    }

    public static BandLineParser of(Text helpMessage, Consumer<String> binder, boolean enforcePattern) {
        return new BandLineParser(helpMessage, binder, enforcePattern);
    }

    public static ParsingConfiguration.LineParser bandName(Consumer<String> binder) {
        return BandLineParser.of(Text.of("Bandname"), binder, false);
    }

    public static ParsingConfiguration.LineParser shortBand(Consumer<String> binder) {
        return BandLineParser.of(Text.of("ShortBand"), binder, false);
    }

    public static ParsingConfiguration.LineParser wideBand(BiConsumer<String, Boolean> binder) {
        return new ParsingConfiguration.LineParser() {
            @Override
            public void parse(String line, @Nullable Subject cause) {
                if (line.isEmpty()) {
                    // Allowed for a wide band.
                    binder.accept(line, false);
                    return;
                }
                line = line.trim();

                boolean shouldLoadLine = false;
                if (line.equalsIgnoreCase("uuid") && cause instanceof Identifiable) {
                    line = ((Identifiable) cause).getUniqueId().toString();
                    shouldLoadLine = true;
                }
                binder.accept(line, shouldLoadLine);
            }

            @Override
            public Text getHelp() {
                return Text.of("Wideband, 'uuid' will convert to your uuid.");
            }
        };
    }
}
