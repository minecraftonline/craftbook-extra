/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.data;

import com.flowpowered.math.vector.Vector3d;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableSingleData;
import org.spongepowered.api.data.value.immutable.ImmutableValue;

public class ImmutableChairExitLocationData extends AbstractImmutableSingleData<Vector3d, ImmutableChairExitLocationData, ChairExitLocationData> {

    public ImmutableChairExitLocationData(Vector3d value) {
        super(CraftBookKeys.CHAIR_EXIT_LOCATION, value, new Vector3d(0, 1, 0));
    }

    @Override
    public ChairExitLocationData asMutable() {
        return new ChairExitLocationData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }

    @Override
    protected ImmutableValue<Vector3d> getValueGetter()
    {
        return Sponge.getRegistry().getValueFactory().createValue(CraftBookKeys.CHAIR_EXIT_LOCATION, getValue()).asImmutable();
    }
}
