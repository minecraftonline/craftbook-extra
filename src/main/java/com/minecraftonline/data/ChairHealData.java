/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.data;

import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractBooleanData;
import org.spongepowered.api.data.merge.MergeFunction;

import java.util.Optional;

public class ChairHealData extends AbstractBooleanData<ChairHealData, ImmutableChairHealData> {

    public ChairHealData(boolean value) {
        super(CraftBookKeys.HEALING_CHAIR, value, false);
    }

    @Override
    public Optional<ChairHealData> fill(DataHolder dataHolder, MergeFunction overlap) {
        dataHolder.get(ChairHealData.class).ifPresent((data) -> {
            ChairHealData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<ChairHealData> from(DataContainer container) {
        return container.getBoolean(CraftBookKeys.HEALING_CHAIR.getQuery()).map(ChairHealData::new);
    }

    @Override
    public ChairHealData copy() {
        return new ChairHealData(this.getValue());
    }

    @Override
    public ImmutableChairHealData asImmutable() {
        return new ImmutableChairHealData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }
}
