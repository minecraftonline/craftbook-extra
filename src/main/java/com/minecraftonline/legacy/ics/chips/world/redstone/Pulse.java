/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.world.redstone;

import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Pulse
 * ModelID: MCX010
 *
 * @author Brendan  (doublehelix457)
 * @author Drathus (Legacy)
 */
public class Pulse extends AbstractIC {

    public Pulse(ICFactory<Pulse> icFactory, Location<World> block){super(icFactory,block);}

    boolean activePulse = false;
    int pulseLength = 250;
    int pulseCount = 1;

    private Task pulseTask;
    private boolean running = false;

   @Override
   public void create(Player player, List<Text> lines) throws InvalidICException{
       super.create(player, lines);
       int pulse = 0;
       int pulseCount = 1;

       try{
           if(SignUtil.getTextRaw(lines.get(2)).length() != 0) {
               pulse = Integer.parseInt(SignUtil.getTextRaw(lines.get(2)));
           }
       }catch(NumberFormatException e){
           throw new InvalidICException("Invalid pulse length, must be a number between 100 and 1000");
       }

       if((pulse < 100 || pulse > 1000) && SignUtil.getTextRaw(lines.get(2)).length() != 0){
           throw new InvalidICException("Invalid pulse length, valid range: 100-1000");
       }

       try{
           if(SignUtil.getTextRaw(lines.get(3)).length() != 0){
               pulseCount = Integer.parseInt(SignUtil.getTextRaw(lines.get(3)));
           }
       }catch(NumberFormatException e){
           throw new InvalidICException("Invalid Pulse Count, must be a number between 1 and 10.");
       }

       if(pulseCount < 1 || pulseCount > 10) throw new InvalidICException("Invalid pulse count, valid range: 1-10");
   }

   //Always safer to get values in the load()
   @Override
   public void load(){
       super.load();
       if(!(getLine(2).length() == 0)) pulseLength = Integer.parseInt(getLine(2));
       if(!(getLine(3).length() == 0)) pulseCount = Integer.parseInt(getLine(3));

   }

   @Override
   public void unload(){
       stopTask();
   }

   private void startTask(){
        if(running) return;
        pulseTask = Task.builder()
                .execute(new PulserTask(this))
                .interval(pulseLength, TimeUnit.MILLISECONDS)
                .name("CBX - Pulse " + UUID.randomUUID())
                .submit(CraftBookPlugin.spongeInst());
        running = true;
   }

   private void stopTask(){
       if(pulseTask != null) pulseTask.cancel();
       running = false;
   }

    @Override
    public void onTrigger() {
        if(getPinSet().isAnyTriggered(this)) startTask();
   }

   private class PulserTask implements Consumer<Task> {
       private final Pulse pulse;
       // the amount of ticks this task was running for
       private int currentPulseCount = 0;
       // the ticks of the current pulse. How long it is running
       private boolean on = false;
       private PulserTask(Pulse pulse){
           this.pulse = pulse;
       }
       @Override
       public void accept(Task task) {
            if(on) stopPulse();
            else startPulse();

            if(currentPulseCount >= pulse.pulseCount) stopTask();
       }

       private void startPulse(){
           if(pulse == null) return;
           pulse.getPinSet().setOutput(0,true,pulse);
           on = true;
       }

       private void stopPulse(){
           if(pulse == null) return;
           pulse.getPinSet().setOutput(0,false,pulse);
           currentPulseCount++;
           on = false;
       }
   }

    public static class Factory implements ICFactory<Pulse>{
        @Override
        public Pulse createInstance(Location<World> location) {return new Pulse(this,location);}

        @Override
        public String[][] getPinHelp() {
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs pulse on input."
                    }
            };
        }
    }
}
